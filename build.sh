#!/bin/sh

cd "$(dirname $0)"

_shasum() {
	find . -not -path '*/.*' -not -path "${buildstamp}" -type f | sha256sum
}

buildstamp="debian/debhelper-build-stamp"
[ -e "${buildstamp}" ] || \
	touch -d '@0' "${buildstamp}"

changed=
if [ -n "${changed:="$(find . -not -path '*/.*' -type f -cnewer "${buildstamp}")"}" ] || \
[ "$(_shasum)" != "$(cat "${buildstamp}")" ]; then
	printf '%s\n' "Changed files:" ${changed} ""
	! debuild -tc || \
		_shasum > "${buildstamp}"
else
	echo "Nothing to do" >&2
fi
